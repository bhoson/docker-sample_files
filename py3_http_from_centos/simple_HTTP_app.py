#!/usr/bin/python3.6
from http.server import BaseHTTPRequestHandler, HTTPServer
 
class SimpleHTTPServer_RequestHandler(BaseHTTPRequestHandler):
  def do_GET(self):
        self.send_response(200)
 
        self.send_header('Content-type','text/html')
        self.end_headers()
 
        message = "Hello world!\n"
        self.wfile.write(bytes(message, "utf8"))
        return
 
def run():
  print('starting server...')
 
  server_address = ('0.0.0.0', 80)
  httpd = HTTPServer(server_address, SimpleHTTPServer_RequestHandler)
  print('running server...')
  httpd.serve_forever()
 
 
run()

