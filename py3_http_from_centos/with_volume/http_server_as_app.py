#!/usr/bin/python3.6
from http.server import BaseHTTPRequestHandler, HTTPServer
import sys
import subprocess as cli
class HTTPServer_RequestHandler_greet(BaseHTTPRequestHandler):
  def do_GET(self):
        self.send_response(200)
 
        self.send_header('Content-type','text/html')
        self.end_headers()
 
        message = "Hello world!"
        self.wfile.write(bytes(message, "utf8"))
        return


class HTTPServer_RequestHandler_info(BaseHTTPRequestHandler):
  def do_GET(self):
        self.send_response(200)

        self.send_header('Content-type','text/html')
        self.end_headers()

        message = str(cli.check_output(["cat","/proc/cpuinfo"]))
        self.wfile.write(bytes(message, "utf8"))
        return

 
def run_greet():
  print('starting server...')
   
  server_address = ('0.0.0.0', 80)
  httpd = HTTPServer(server_address, HTTPServer_RequestHandler_greet)
  print('running server...')
  httpd.serve_forever()
 
def run_info():
  print('starting server...')
   
  server_address = ('0.0.0.0', 80)
  httpd = HTTPServer(server_address, HTTPServer_RequestHandler_info)
  print('running server...')
  httpd.serve_forever()

def run_script():
  if len (sys.argv) == 2 :
    arg = sys.argv[1]
    if arg == "--greet": 
      run_greet()
    elif arg == "--info":
      run_info()
  else:
    run_greet()


run_script()

